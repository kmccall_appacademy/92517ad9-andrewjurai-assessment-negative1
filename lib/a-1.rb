# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  total = (nums.min..nums.max)
  total.select { |int| int unless nums.include?int }
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  digits = digit_array(binary)
  digit_base = digits.map.with_index { |int, i| int * 2**i }
  digit_base.reduce(:+)
end

def digit_array(binary)
  binary.to_s.chars.map(&:to_i).reverse
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    return_hash = Hash.new
    self.each do  |k, v|
      if prc.call(k, v)
        return_hash[k] = v
      end
    end
    return_hash
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    return_hash = Hash.new
    return_hash = self.merge_hsh(hash)

    if block_given?
      possible_args = self.to_a.map do |nest|
        if hash.key?(nest[0])
          nest << hash[nest[0]]
        end
        nest
      end.compact
      possible_args.each do |nest|
        next if nest.length != 3
        return_hash[nest[0]] = prc.call(*nest)
      end

    end #find possible arguments to pass to block of the form
    #[k,v,v]. Pass it to the block and return a hash with the  first arg as the
    #key and the value of the block as the value
    return_hash
  end

  def merge_hsh(hash1)
    return_hash = self.dup
    hash1.each { |k, v| return_hash[k] = v }
    return_hash
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  return 2 if n == 0
  posit_series = [2, 1]
  neg_series = [-1, 2]

  if positive_index?(n)
    until posit_series.length == n + 1
      posit_series << if_posit_next_num_in(posit_series)
    end
    return posit_series.last
  else
    until neg_series.length == n.abs + 1
      next_val = if_nega_next_num_in(neg_series)
      neg_series.unshift(next_val)
    end
    return neg_series.first
  end
end

def if_nega_next_num_in(series)
  series[0..1].reduce { |acc, int| int - acc }
end

def if_posit_next_num_in(series)
  series[-2..-1].reduce(:+)
end

def positive_index?(n)
  n >= 0
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  counter1 = 0
  counter2 = 0
  palindrome = Array.new
  until counter1 == string.length
    until counter2 == string.length
      counter1
      counter2
      if string[counter1..counter2].palindrome_of?(string)
        palindrome << string[counter1..counter2]
      end
      counter2 += 1
    end
    counter1 += 1
    counter2 = 0
  end
  longest_pal = palindrome.max_by { |str| str.length }
  return false if longest_pal.length <= 2
  longest_pal.length
end

class String
  def palindrome_of?(string)
    string.reverse.include?(self)
  end
end
